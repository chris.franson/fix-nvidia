#!/home/chris/.pyenv/shims/python

import os
import datetime
from functools import partial


from simple_term_menu import TerminalMenu


interesting_wakeup_devices = ["GLAN", "EHC1", "EHC2", "XHC"]
usb_wakeup_devices = ["USB"+str(i) for i in range(1,8)]
interesting_wakeup_devices += usb_wakeup_devices


def main():
    options = {
        "Custom command": custom_command,
        # "Auto fix": auto_fix,
        "Show system config summary": show_system_config_summary,
        "Show display info": partial(call, "xrandr"),
        "Show all config files": show_all_config_files,
        "Wakeup Devices": wakeup_devices,
        "Nvidia": nvidia,
        "Nouveau": nouveau,
        "Edit /etc/default/grub": edit_etc_default_grub,
        "Back-up and update kernel": back_up_and_update_kernel,
        "Add Hibernate menu item": partial(call, "sudo cp /home/chris/storage/Projects/System/Suspend/com.ubuntu.desktop.pkla /var/lib/polkit-1/localauthority/10-vendor.d/"),
        "Remove Hibernate menu item": partial(call, "sudo rm /var/lib/polkit-1/localauthority/10-vendor.d/com.ubuntu.desktop.pkla"),
        "Clear screen": partial(call, "clear"),
        "Reboot": partial(call, "reboot"),
        "Suspend": partial(call, "systemctl suspend"),
        "Sudo Reboot": partial(call, "sudo reboot"),
        # "Call a function with args": partial(function, "arg1"),
        # "": ,
        "Quit": quit,
    }
    main_menu = TerminalMenu(options, multi_select=True)
    menu_entry_indices = main_menu.show()
    for entry in main_menu.chosen_menu_entries:
        options[entry]()
    main()


def nvidia():
    options = {
        "Install nvidia:515 with ubuntu-drivers": partial(call, "sudo ubuntu-drivers install nvidia:515"),
        "Obliterate Nvidia": obliterate_nvidia,
	    "Check for anything nvidia installed": partial(call, "sudo apt list --installed | grep -i nvidia"),
        "List nvidia sleep services": partial(call, 'find /etc/systemd/ -iname "nv*"'),
        "Disable nvidia sleep services": disable_nvidia_sleep_services,
        "Enable nvidia sleep services": enable_nvidia_sleep_services,
        "Edit nvidia-sleep.sh with nano": partial(call, "sudo nano /usr/bin/nvidia-sleep.sh"),
        "Show /etc/modprobe.d/nvidia-power-management.conf": partial(call, 'sudo cat /etc/modprobe.d/nvidia-power-management.conf'),
        "Add /etc/modprobe.d/nvidia-power-management.conf": partial(call, 'sudo bash -c "echo options nvidia NVreg_PreserveVideoMemoryAllocations=1 > /etc/modprobe.d/nvidia-power-management.conf" && cat /etc/modprobe.d/nvidia-power-management.conf'),
        "Remove /etc/modprobe.d/nvidia-power-management.conf": partial(call, 'sudo rm /etc/modprobe.d/nvidia-power-management.conf'),
        "Back to Main Menu": True,
    }
    main_menu = TerminalMenu(options, multi_select=True)
    menu_entry_indices = main_menu.show()
    for entry in main_menu.chosen_menu_entries:
        if entry == "Back to Main Menu":
            return
        options[entry]()
    nvidia()


def wakeup_devices():
    options = {
        "Show all wakeup devices": partial(call, "cat /proc/acpi/wakeup"),
        "Show interesting wakeup devices": show_interesting_wakeup_devices,
        "Toggle wakeup devices": toggle_wakeup_devices,
        "Show disable-wakeup-devices.service status": partial(call, "systemctl status disable-wakeup-devices.service"),
        "Back to Main Menu": True,
    }
    main_menu = TerminalMenu(options, multi_select=True)
    menu_entry_indices = main_menu.show()
    for entry in main_menu.chosen_menu_entries:
        if entry == "Back to Main Menu":
            return
        options[entry]()
    main()


def nouveau():
    options = {
        "Install nouveau driver with 'ubuntu-drivers'": partial(call, "sudo ubuntu-drivers install xserver-xorg-video-nouveau"),
        "Install nouveau driver with apt": partial(call, "sudo apt install xserver-xorg-video-nouveau"),
        "Disable Nouveau kernel driver": partial(call, 'sudo bash -c "echo blacklist nouveau > /etc/modprobe.d/blacklist-nvidia-nouveau.conf" && sudo bash -c "echo options nouveau modeset=0 >> /etc/modprobe.d/blacklist-nvidia-nouveau.conf"'),
        "Enable Nouveau kernel driver": partial(call, "sudo rm /etc/modprobe.d/blacklist-nvidia-nouveau.conf"),
        "Remove Nouveau driver": partial(call, "sudo apt remove --purge xserver-xorg-video-nouveau"),
        "Back to Main Menu": True,
    }
    menu = TerminalMenu(options, multi_select=True)
    menu_entry_indices = menu.show()
    for entry in menu.chosen_menu_entries:
        if entry == "Back to Main Menu":
            return
        options[entry]()
    nouveau()


def custom_command():
    print("\n\n")
    command = input("$ ")
    call(command, print_it=False)
    print("\n\n")


def obliterate_nvidia():
    print("\n\n\n\nremoving everything nvidia")
    call("sudo apt remove -y --purge '^nvidia-.*'")
    call("sudo apt autoremove -y $(dpkg -l xserver-xorg-video-nvidia* | grep ii | awk '{print $2}')")
    call("sudo apt remove -y libnvidia-compute-* linux-objects-nvidia-* linux-signatures-nvidia-*")
    call("sudo apt autoremove -y")
    call("sudo rm /etc/modprobe.d/nvidia-power-management.conf")
    disable_nvidia_sleep_services()
    if confirmed("Install and enable the nouveau driver?"):
        call("sudo rm /etc/modprobe.d/blacklist-nvidia-nouveau.conf")
        call("sudo ubuntu-drivers install xserver-xorg-video-nouveau")
    back_up_and_update_kernel()


def show_system_config_summary():
    print("\n\nSorry, not yet.\n\n")


def show_all_config_files():
    print("\n\nInteresting devices in /proc/acpi/wakeup:")
    show_interesting_wakeup_devices()
    print("\n")
    call('sudo find /etc/systemd/ -iname "nv*"')
    print("\n")
    call("sudo cat /usr/bin/nvidia-sleep.sh")
    print("\n\n")
    call("sudo cat /etc/modprobe.d/nvidia-power-management.conf")
    print("\n")
    call("sudo cat /etc/default/grub")
    print("\n")
    call("sudo cat /etc/modprobe.d/blacklist-nvidia-nouveau.conf")
    print("\n")
    call("systemctl status disable-wakeup-devices.service")
    print("\n")
    call("sudo cat /var/lib/polkit-1/localauthority/10-vendor.d/com.ubuntu.desktop.pkla")
    print("\n\n\n\n")


def auto_fix():
    print("\n\n")
    call("echo XHC | sudo tee /proc/acpi/wakeup")
    call("sudo cp /storage/Projects/System/Nvidia/disable-wakeup-devices.service /etc/systemd/system/")
    call("./storage/Projects/System/Nvidia/disable-wakeup-devices.sh")
    # disable_nvidia_sleep_services()
    print("\n\nOK, now reboot and/or suspend to test.\n\n")


def show_interesting_wakeup_devices():
    for device in interesting_wakeup_devices:
        call(f"cat /proc/acpi/wakeup | grep {device}", print_it=False)


def toggle_wakeup_devices():
    menu = TerminalMenu(interesting_wakeup_devices, multi_select=True)
    menu.show()
    for entry in menu.chosen_menu_entries:
        call(f"echo {entry} | sudo tee /proc/acpi/wakeup")


def disable_nvidia_sleep_services():
    call("sudo rm /etc/systemd/system/systemd-hibernate.service.wants/nvidia-hibernate.service")
    call("sudo rm /etc/systemd/system/systemd-hibernate.service.wants/nvidia-resume.service")
    call("sudo rm /etc/systemd/system/systemd-suspend.service.wants/nvidia-suspend.service")
    call("sudo rm /etc/systemd/system/systemd-suspend.service.wants/nvidia-resume.service")


def enable_nvidia_sleep_services():
    call("sudo ln -s /lib/systemd/system/nvidia-hibernate.service /etc/systemd/system/systemd-hibernate.service.wants/nvidia-hibernate.service")
    call("sudo ln -s /lib/systemd/system/nvidia-resume.service /etc/systemd/system/systemd-hibernate.service.wants/nvidia-resume.service")
    call("sudo ln -s /lib/systemd/system/nvidia-suspend.service /etc/systemd/system/systemd-suspend.service.wants/nvidia-suspend.service")
    call("sudo ln -s /lib/systemd/system/nvidia-resume.service /etc/systemd/system/systemd-suspend.service.wants/nvidia-resume.service")


def edit_etc_default_grub():
    call("sudo nano /etc/default/grub")
    if confirmed("Want to run `sudo update-grub` now?"):
        call("sudo update-grub")


def back_up_and_update_kernel():
    current_time = datetime.datetime.now()
    timestamp = current_time.strftime("%Y-%m-%d_%H-%M-%S")
    call(f"sudo cp /boot/initrd.img-$(uname -r) /home/chris/storage/Projects/System/Suspend/Backups/initrd.img-$(uname -r)_{timestamp}")
    # call("sudo update-initramfs -uv")
    call("sudo update-initramfs -u")


def quit():
    exit()


def confirmed(question):
    user_input = input(question + ' [y]/n/q ')
    if user_input.lower() == 'q':
        print("\n\nExiting.\n\n")
        exit()
    return user_input.lower() == 'y' or user_input.lower() == 'yes' or user_input == ''


def call(command, print_it=True, verbose_message=None):
    if verbose_message:
        print(f"\n\n{verbose_message}\n")
    if print_it:
        print(command)
    os.system(command)


if __name__ == "__main__":
    main()

