# fix-nvidia.py

This simple little script contains CLI menus to serve as shortcuts that might help you fix the issues 
you're having with the Nvidia drivers for Linux, particularly those related to hibernate and suspend.
I wrote and used it because I couldn't stand typing the commands and/or up-arrowing through my bash
history to repeat them over and over, looking for that magical combination of tricks to finally get
the drivers to behave.

## Requirements

 - Python 3
 - [simple-term-menu](https://pypi.org/project/simple-term-menu/)

## Installation

 1. Clone this repository, download fix-nvidia.py, or copy-paste the code into a file on your computer.
 2. (Optionally) Create a new python virtual environment in which to install dependencies related to your
quest to fix your Nvidia GPU issues. 
 3. `pip install simple-term-menu`

## Usage
 1. `python fix-nvidia.py`
 2. Use the arrow keys to navigate the menus, and the Enter key to select an item.

## Contributing

This script is mostly just an expression of my frustration and disappointment. When more issues came up
after an Ubuntu update (kernel and/or minor Nvidia driver update), I finally resolved all of my issues
by purchasing an AMD card.

If you want to publish your changes to this script in order to assist your fellow unrequited Nvidia users,
I recommend that you fork this repo, as unless Nvidia releases something completely mindblowing, like
open-source drivers, I'm unable to expend any more time on this project.

## License

This project is licensed under the [GNU GPL v3](https://www.gnu.org/licenses/gpl-3.0).
